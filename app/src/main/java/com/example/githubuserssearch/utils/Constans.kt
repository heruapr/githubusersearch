package com.example.githubuserssearch.utils

import android.content.Context
import android.view.Gravity
import android.widget.Toast

object Constants {
    val TAG = "GitubUsersSearch"
    val BASE_URL = "https://api.github.com/"
    val USER_OFFLINE = "Check your internet connection!"
    val USER_NOT_FOUND = "User not found!"
    val EMPTY_FIELD = "Please input username"
    val ACCESS_TOKEN_GITHUB = ""
    //token already added in apk debug
    //I dont include it in this code

    fun toast(context: Context, message: String) =
        Toast.makeText(context, message, Toast.LENGTH_SHORT).setGravity(Gravity.CENTER, 0, 0)

}