package com.example.githubuserssearch.api

import android.app.Application
import com.example.githubuserssearch.di.repositoryModule
import com.example.githubuserssearch.di.uiModule
import com.example.githubuserssearch.utils.Constants
import com.example.githubuserssearch.utils.Constants.BASE_URL
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory


class RestApiFactory : Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            //inject Android context
            androidContext(this@RestApiFactory)
            // use modules
            modules(
                listOf(
                    repositoryModule,
                    uiModule
                )
            )
        }
    }

    private val client = OkHttpClient().newBuilder()
        .addInterceptor { chain ->
            val request = chain.request()
                .newBuilder()
                .addHeader("Authorization", "token "+Constants.ACCESS_TOKEN_GITHUB)
                .build()
            chain.proceed(request)
        }

    var gson = GsonBuilder()
        .setLenient()
        .create()

    private val retrofit = Retrofit.Builder()
        .baseUrl(BASE_URL) //base URL disimpan di object Constant.kt
        .client(client.build())
        .addConverterFactory(GsonConverterFactory.create(gson))
        .build()

    val services: ApiInterface = retrofit.create(ApiInterface::class.java)
}