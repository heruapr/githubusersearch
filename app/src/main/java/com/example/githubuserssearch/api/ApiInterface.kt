package com.example.githubuserssearch.api

import com.example.githubuserssearch.model.*
import retrofit2.Call
import retrofit2.http.*

interface ApiInterface {
    @GET("search/users")
    fun getUsers(@Query("q") q: String, @Query("page") page: Int): Call<BaseResponse<User>>
}