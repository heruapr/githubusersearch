package com.example.githubuserssearch.model

import com.google.gson.annotations.SerializedName

data class BaseResponse<User>(
    @SerializedName("total_count") var total_count: Int,
    @SerializedName("incomplete_results") var incomplete_results: Boolean,
    @SerializedName("message") var message: String,
    @SerializedName("items") var items: List<User>
) {
}