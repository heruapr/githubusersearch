package com.example.githubuserssearch.viewmodel

import android.app.Activity
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.example.githubuserssearch.model.User
import com.example.githubuserssearch.repository.Repository
import com.example.githubuserssearch.repository.UserDataSourceFactory

class ItemViewModel : ViewModel() {
    lateinit var activity: Activity
    lateinit var q: String
    lateinit var userPagedList: LiveData<PagedList<User>>
    lateinit var liveDataSource: LiveData<Repository>

    fun getUser(q: String, activity: Activity) {
        //init {
        val itemDataSourceFactory = UserDataSourceFactory(q, activity)
        liveDataSource = itemDataSourceFactory.userLiveDataSource
        val config = PagedList.Config.Builder()
            .setEnablePlaceholders(false)
            .setPageSize(Repository.PAGE_SIZE)
            .build()
        userPagedList = LivePagedListBuilder(itemDataSourceFactory, config)
            .build()
        //}
    }

    fun retryPagination(q: String, activity: Activity) {
        Repository(q, activity).retryPagination()
    }
}