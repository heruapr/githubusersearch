package com.example.githubuserssearch.repository

import android.app.Activity
import android.util.Log
import android.view.Gravity
import android.view.View
import android.widget.Toast
import androidx.paging.PageKeyedDataSource
import com.example.githubuserssearch.api.RestApiFactory
import com.example.githubuserssearch.model.BaseResponse
import com.example.githubuserssearch.model.User
import com.example.githubuserssearch.ui.activity.MainActivity
import kotlinx.android.synthetic.main.activity_main.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class Repository(q: String, activity: Activity) : PageKeyedDataSource<Int, User>() {

    val q: String = q
    val activity: Activity = activity

    // For Retry
    private var paramsAfter: LoadParams<Int>? = null
    private var callbackAfter: LoadCallback<Int, User>? = null

    private var params: LoadInitialParams<Int>? = null
    private var callback: LoadInitialCallback<Int, User>? = null

    override fun loadInitial(
        params: LoadInitialParams<Int>,
        callback: LoadInitialCallback<Int, User>
    ) {
        this.params = params
        this.callback = callback

        RestApiFactory().services.getUsers(q, FIRST_PAGE)
            .enqueue(object : Callback<BaseResponse<User>> {
                override fun onResponse(
                    call: Call<BaseResponse<User>>,
                    response: Response<BaseResponse<User>>
                ) {
                    if (response.isSuccessful) {
                        val apiResponse = response.body()!!
                        val responseItems = apiResponse.items
                        responseItems?.let {
                            callback.onResult(responseItems, null, FIRST_PAGE + 1)
                        }
                        activity.img_not_found.visibility =  View.GONE
                        if (response.body()?.total_count == 0) {
                            val toast: Toast = Toast.makeText(
                                activity.applicationContext,
                                "User Not Found!", Toast.LENGTH_SHORT
                            )
                            activity.img_not_found.visibility =  View.VISIBLE
                            toast.setGravity(Gravity.CENTER, 0, 0)
                            toast.show()

                        } else if (response.headers().get("X-RateLimit-Remaining") == "0") {
                            val toast: Toast = Toast.makeText(
                                activity.applicationContext,
                                "Request Limit! please wait 1 minute to reset",
                                Toast.LENGTH_SHORT
                            )
                            toast.setGravity(Gravity.CENTER, 0, 0)
                            toast.show()
                        }
                    }
                }

                override fun onFailure(call: Call<BaseResponse<User>>, t: Throwable) {
                }
            })
    }

    override fun loadAfter(params: LoadParams<Int>, callback: LoadCallback<Int, User>) {
        Log.d("loaded", "loaded loadafter")
        this.paramsAfter = params
        this.callbackAfter = callback

        RestApiFactory().services.getUsers(q, params.key)
            .enqueue(object : Callback<BaseResponse<User>> {
                override fun onResponse(
                    call: Call<BaseResponse<User>>,
                    response: Response<BaseResponse<User>>
                ) {
                    if (response.isSuccessful) {
                        val apiResponse = response.body()!!
                        val responseItems = apiResponse.items
                        val key = params.key + 1
                        responseItems?.let {
                            callback.onResult(responseItems, key)
                        }
                    }
                }

                override fun onFailure(call: Call<BaseResponse<User>>, t: Throwable) {
                }
            })
    }

    fun retryPagination() {
        if (paramsAfter != null) {
            loadAfter(paramsAfter!!, callbackAfter!!)
        } else {
            (activity as MainActivity).getUser(activity.field_search.text.toString())
        }
    }

    override fun loadBefore(params: LoadParams<Int>, callback: LoadCallback<Int, User>) {
        RestApiFactory().services.getUsers(q, params.key)
            .enqueue(object : Callback<BaseResponse<User>> {
                override fun onResponse(
                    call: Call<BaseResponse<User>>,
                    response: Response<BaseResponse<User>>
                ) {
                    if (response.isSuccessful) {
                        val apiResponse = response.body()!!
                        val responseItems = apiResponse.items
                        val key = if (params.key > 1) params.key - 1 else 0
                        responseItems?.let {
                            callback.onResult(responseItems, key)
                        }
                    }
                }

                override fun onFailure(call: Call<BaseResponse<User>>, t: Throwable) {
                }
            })
    }

    companion object {
        const val PAGE_SIZE = 10
        const val FIRST_PAGE = 1
    }
}
