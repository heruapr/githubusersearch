package com.example.githubuserssearch.repository

import android.app.Activity
import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import com.example.githubuserssearch.model.User

class UserDataSourceFactory(q: String, activity: Activity) : DataSource.Factory<Int, User>() {
    var q: String = q
    var activity:Activity = activity
    val userLiveDataSource = MutableLiveData<Repository>()
    override fun create(): DataSource<Int, User> {
        val userDataSource = Repository(q, activity)
        userLiveDataSource.postValue(userDataSource)
        return userDataSource
    }
}