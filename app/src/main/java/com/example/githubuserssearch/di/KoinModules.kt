package com.example.githubuserssearch.di

import android.app.Activity
import com.example.githubuserssearch.repository.Repository
import com.example.githubuserssearch.ui.activity.MainActivity
//import com.example.githubuserssearch.ui.adapter.ItemAdapter
import com.example.githubuserssearch.viewmodel.ItemViewModel
import com.example.mywallet.ui.adapter.ItemAdapter
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val repositoryModule = module {
    single {
        Repository(get(), get())
    }
}
val uiModule = module {
    factory { ItemAdapter() }
    factory { MainActivity() }
    viewModel { ItemViewModel() }
}