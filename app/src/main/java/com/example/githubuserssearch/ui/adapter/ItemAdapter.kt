package com.example.mywallet.ui.adapter

import android.content.Context
import android.os.Build
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.annotation.GlideModule
import com.bumptech.glide.module.AppGlideModule
import com.example.githubuserssearch.R
import com.example.githubuserssearch.model.User
import com.example.githubuserssearch.ui.activity.MainActivity
import com.example.githubuserssearch.utils.Constants


class ItemAdapter : PagedListAdapter<User, ItemAdapter.ViewHolder>(USER_COMPARATOR) {
    private lateinit var list: ArrayList<User>

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.item, parent, false)
        return ViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
//        val item = list[position]
        val item = getItem(position)
        holder.textViewUsername.text = item?.login
        Glide.with(holder.itemView).load(item?.avatar_url).into(holder.imageViewAvatar)
    }

//    override fun getItemCount(): Int {
//        return list.size
//    }

//    fun setList(list: ArrayList<User>) {
//        this.list = list
//        notifyDataSetChanged()
//    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val textViewUsername: TextView = itemView.findViewById(R.id.textview_username)
        val imageViewAvatar: ImageView = itemView.findViewById(R.id.avatar)
    }

    companion object {
        private val USER_COMPARATOR = object : DiffUtil.ItemCallback<User>() {
            override fun areItemsTheSame(oldItem: User, newItem: User): Boolean =
                oldItem.id == newItem.id

            override fun areContentsTheSame(oldItem: User, newItem: User): Boolean =
                newItem == oldItem
        }
    }
}