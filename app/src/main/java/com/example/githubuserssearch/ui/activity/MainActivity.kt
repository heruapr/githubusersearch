package com.example.githubuserssearch.ui.activity

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.Gravity
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.githubuserssearch.R
import com.example.githubuserssearch.utils.Constants
import com.example.githubuserssearch.viewmodel.ItemViewModel
import com.example.mywallet.ui.adapter.ItemAdapter
import kotlinx.android.synthetic.main.activity_main.*
import org.koin.android.ext.android.inject


class MainActivity : AppCompatActivity() {
    private val viewModel: ItemViewModel by inject()
    private val adapter: ItemAdapter by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        field_search.addTextChangedListener(textWatcher)
        recyclerview.layoutManager = LinearLayoutManager(this)
    }

    fun getUser(username: String) {
        if (!isOnline(this@MainActivity)) {
            img_not_found.visibility = View.GONE
            img_not_connected.visibility = View.VISIBLE
            val toast: Toast = Toast.makeText(
                this,
                Constants.USER_OFFLINE, Toast.LENGTH_SHORT
            )
            toast.setGravity(Gravity.CENTER, 0, 0)
            toast.show()
            btn_refresh.visibility = View.VISIBLE
            btn_refresh.setOnClickListener {
                viewModel.retryPagination(username, this@MainActivity)
            }
        } else {
            img_not_connected.visibility = View.GONE
            btn_refresh.visibility = View.GONE
            viewModel.getUser(username, this@MainActivity)
            viewModel.userPagedList.observe(this, Observer {
                adapter.submitList(it)
            })
            recyclerview.adapter = adapter
        }
    }

    private val textWatcher: TextWatcher = object : TextWatcher {
        override fun onTextChanged(
            s: CharSequence,
            start: Int,
            before: Int,
            count: Int
        ) { //after text changed

        }

        override fun beforeTextChanged(
            s: CharSequence, start: Int, count: Int,
            after: Int
        ) {
        }

        override fun afterTextChanged(s: Editable) {
            getUser(s.toString())
        }
    }

    fun isOnline(context: Context): Boolean {
        val connectivityManager =
            context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        if (connectivityManager != null) {
            val capabilities =
                connectivityManager.getNetworkCapabilities(connectivityManager.activeNetwork)
            if (capabilities != null) {
                if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR)) {
                    Log.i("Internet", "NetworkCapabilities.TRANSPORT_CELLULAR")
                    return true
                } else if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI)) {
                    Log.i("Internet", "NetworkCapabilities.TRANSPORT_WIFI")
                    return true
                } else if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET)) {
                    Log.i("Internet", "NetworkCapabilities.TRANSPORT_ETHERNET")
                    return true
                }
            }
        }
        return false
    }
}
